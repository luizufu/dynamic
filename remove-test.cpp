#include "dynamic/dynamic.hpp"

template <typename BitVector> void print(const BitVector &bits) {
  uint32_t counter = 1;
  bool last = bits.at(0);
  for (uint32_t i = 1; i < bits.size(); ++i) {
    if (bits.at(i) == last) {
      ++counter;
    } else {
      std::cout << static_cast<int>(last) << " (" << counter << ")  ->  ";
      counter = 1;
      last = !last;
    }
  }
  std::cout << static_cast<int>(last) << " (" << counter << ")" << std::endl;
}

int main() {
  dyn::suc_bvbr b1;
  while (b1.size() < 8192 * 16 * 16 * 16) {
    b1.push_word(0UL - 1, 64);
  }
  print(b1);
  b1.clear_n(1, 8192 * 16 * 16 * 16 - 2);
  print(b1);

  return 0;
}
